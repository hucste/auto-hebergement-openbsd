
function changeTitle() {
	var timeout_title;
	var headers = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
	var isread = document.getElementById("isread");

	window.addEventListener('scroll', function(){
		clearTimeout(timeout_title);
		timeout_title = setTimeout(function(){
			for (var i =0; i < headers.length; i++) {
				if ( document.documentElement.scrollTop + 50 >= headers[i].offsetTop ) {
					isread.innerHTML = headers[i].innerHTML + 
						'<span class="tocarrow">&gt;</span>';
				}
			}
		}, 100);
	});
};


window.addEventListener("load", changeTitle); 
