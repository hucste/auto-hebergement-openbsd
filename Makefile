all:
	@txt2tags -t xhtml -o auto-hebergement-openbsd.html src/auto-hebergement-openbsd.t2t 
	@sed -i "s;\.\.\.;…;g" auto-hebergement-openbsd.html
pdf:
	@txt2tags -t tex -o auto-hebergement-openbsd.tex src/auto-hebergement-openbsd.t2t 
	@pdflatex auto-hebergement-openbsd.tex 

