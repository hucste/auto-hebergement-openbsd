Monit
mwolff44, thuban
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


=== Monit ===

Monit est un outil léger open source (licence AGPL) permettant de superviser un système Unix. Il est capable d’exécuter des actions en cas de détection de défaillance ou de dépassement de seuil.
Une interface web permet de superviser en temps réel les différents services paramétrés.

==== Installation ====


Commençons par installer le nécessaire et activer le démon monit : 

```
# pkg_add monit
# rcctl enable monit
```

==== Configuration ====


Ensuite, la configuration centrale de Monit se fait au sein du fichier
``monitrc`` localisé sous ``/etc/``. Dans ce fichier, vous pouvez notamment modifier l’adresse email qui recevra les alertes.

Enfin, les configurations complémentaires sont dans le répertoire
``/etc/monit.d/``. 

Nous allons maintenant configurer le serveur d'administration sur un socket TCP
local. Éditez le fichier ``/etc/monitrc`` puis décommentez tout en bas la ligne 

```
include /etc/monit.d/*
```

Nous pouvons maintenant procéder à la configuration en ajoutant des fichiers
dans le dossier ``/etc/monit.d`` qu'il faut créer : 

```
# mkdir -p /etc/monit.d
```


Placez dans ce dossier un fichier que l'on appellera par exemple ``admin.conf``.


```
set daemon  60 with start delay 240
set logfile syslog facility log_daemon
set mailserver localhost
set mail-format {
from: monit@$HOST
subject: Monit Alert -- $SERVICE $EVENT --
message:
Hostname:       $HOST
Service:        $SERVICE
Action:         $ACTION
Date/Time:      $DATE
Info:           $DESCRIPTION
}

set alert root@localhost
```

L'instruction set daemon permet de définir la durée d'un "cycle" monit. Un cycle
correspond à l'intervalle (en secondes) entre deux vérifications. Dans cet
exemple, il commence à analyser votre serveur toutes les minutes après un délai
de 4 minutes.

Cette partie permet de préciser que monit devra enregistrer ses journaux dans
les logs système. Ensuite, on configure l'apparence du mail que monit enverra à
l'adresse ``root@localhost``. Assurez-vous d'avoir fait en sorte de 
[recevoir les mails #admin] que votre serveur est susceptible de vous envoyer.

Avant de lancer Monit, vérifiez bien que la syntaxe des fichiers de
configuration est bonne avec la commande :

```
# monit -t
```
Vous devez obtenir une belle réponse « Control file syntax OK ».

En cas de dysfonctionnement, Monit vous enverra un mail d’alerte (attention à bien configurer votre mail et serveur dans le fichier monitrc).

==== Lancement de monit ====

Le démarrage de monit est simple :

```
# rcctl start monit
```

==== Utilisation ====

La commande suivante permet d'obtenir le status détaillé de votre serveur :

```
# monit status
Monit uptime: 0m
System 'openheb.fritz.box'
  status                       Running
  monitoring status            Monitored
  monitoring mode              active
  on reboot                    start
  load average                 [0.08] [0.11] [0.08]
  cpu                          0.1%us 0.1%sy
  memory usage                 81.1 MB [4.0%]
  swap usage                   0 B [0.0%]
  uptime                       2d 21h 30m
  boot time                    Tue, 14 Mar 2017 19:06:34
  data collected               Fri, 17 Mar 2017 16:37:25

```


Il est possible de consulter l'interface web de Monit sur votre serveur car il
intègre un petit service http. Cependant, ce n'est pas très pratique car cela
suppose que vous ouvrez les ports de votre serveur vers Monit...

À la place, je vous propose de passer par un tunnel SSH. Sur votre ordinateur de
bureau, créez un tunnel qui va "relier" le port de Monit vers le port ``9999``
de votre ordinateur : 

```
$ ssh -L 9999:127.0.0.1:2812 -p 222 toto@&NDD
```

Vous pouvez maintenant ouvrir dans un navigateur sur votre ordinateur la page
http://127.0.0.1:9999 afin de consulter Monit.

==== Configuration avancée des alertes ====

Monit peut aussi envoyer des emails d'alerte en cas d'utilisation de ressources
anormales, comme une mémoire vive faible ou un taux d'utilisation du CPU trop important.

Nous allons maintenant compléter notre configuration afin de surveiller la
charge de notre serveur. Pour cela, nous allons ajouter les lignes suivantes à
notre fichier de configuration:

```
check system localhost
    if loadavg (1min) > 4 then alert
    if loadavg (5min) > 2 then alert
    if memory usage > 75% then alert
    if swap usage > 25% then alert
    if cpu usage (user) > 70% then alert
    if cpu usage (system) > 30% then alert
    if cpu usage (wait) > 20% then alert

```

Puis, il faut recharger la configuration :

```
# rcctl reload monit
```

Ainsi, une alerte par courriel sera envoyée dès que le système utilisera plus de
30% des capacités du ou des processeurs.

Plus d'infos, sur le [site de Monit https://www.mmonit.com/monit/]. L'ensemble
des applications de votre serveur peuvent être facilement monitorées. Regardez
les exemples [à la fin du document #monitrc].


