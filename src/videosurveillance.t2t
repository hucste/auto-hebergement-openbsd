Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Vidéosurveillance==
Que ce soit pour diffuser en direct une vidéo, observer des animaux sans les déranger 
ou découvrir lequel de vos enfants se lève la nuit pour finir le pot de
confiture, vous avez l'embarras du choix pour mettre en place de la
"vidéosurveillance".

Ici, on va décrire quelques méthodes de façon non-exhaustive. Pour l'exemple,
c'est une webcam USB qui est utilisée.

===Captures régulières avec fswebcam===
fswebcam est un capable de prendre des photos avec votre webcam. Simple mais
efficace.

Pour l'installer, on lance comme d'habitude : 

```
# pkg_add fswebcam
```

La commande suivante permet d'enregistrer une image toutes les minutes (60
secondes) : 

```
while true; do fswebcam -r 1280x960 --save "/tmp/webcam-$(date +%Y-%m-%d-%H-%M-%S).jpg
```

Si vous vous placez dans un dossier [accessible par httpd par exemple #httpdadv]
plutôt que dans ``/tmp``, vous
pourrez consulter ces images à distance. Sinon, récupérez-les avec 
[filezilla #filezilla].


===Détecteur de mouvements avec motion===
Le paquet ``motion`` contient de quoi prendre en photo ce qui bouge devant votre
webcam, sinon la garder éteinte.
De plus, il diffuse sur une page web spéciale les images prises ce qui donne
l'effet d'une vidéo.

Installez le paquet ``motion`` puis copiez l'exemple de configuration fourni
après avoir créé le dossier de configuration : 

```
# mkdir -p /etc/motion
# cp /usr/local/share/examples/motion/motion-dist.conf /etc/motion/motion.conf
```


Éditez ce nouveau fichier pour l'adapter à vos besoins. Pour ma part, je n'ai eu
à modifier que les lignes suivantes : 

```
width 1280
height 960
auto_brightness on

# sensibilite du detecteur avant de capturer
threshold 500

ffmpeg_video_codec mp4

target_dir /var/motion
ipv6_enabled true

stream_localhost off
stream_auth_method 2

# Changer les identifiants
stream_authentication utilisateur:motdepasse 

webcontrol_port 0

```


On crée un utilisateur ``_motion`` pour séparer les privilèges.

```
# useradd -s /sbin/nologin _motion
```


Vérifiez que le dossier qui servira à enregistrer les images existe bien et est
accessible par l'utilisateur ``_motion``: 

```
# install -d -o _motion /var/motion/
```

Vous êtes maintenant en mesure de lancer motion :

```
# rcctl enable motion
# rcctl start motion
```


Par défaut, le flux est disponible à l'adresse : ``http://votreserveur:8081``.
Pensez à [ouvrir le port 8081 #pf] avant d'y aller.

Notez que si vous ne souhaitez pas garder en mémoire les enregistrements, vous
pouvez mettre ces options : 

```
snapshot_filename lastname
picture_filename motioncapture
```


En cas d'erreur dans ``/var/log/messages``, vérifiez les permissions sur la
webcam qui doit être accessible en lecture et écriture : 

```
# chmod 666 /dev/video0
```


===Streaming avec ffmpeg===
ffmpeg est un excellent outil. Il nécessite de bonnes performances 
matérielles mais donnera des vidéos de qualité.

Pour l'installer, vous connaissez la chanson : 

```
# pkg_add ffmpeg
```


Tout d'abord, nous avons besoin d'en savoir un peu plus sur notre webcam. Après
l'avoir branchée, tapez ``dmesg`` pour voir par exemple : 


```
uvideo0 at uhub2 port 2 configuration 1 interface 0 "Logitech product 0x0825" rev 2.00/0.12 addr 3
video0 at uvideo0
```


Cela nous indique que la webcam est disponible via ``/dev/video0``. Afin de
pouvoir s'en servir sans être super-utilisateur, modifiez les permissions : 

```
# chmod 666 /dev/video0
```


Une fois que ceci est connu, nous avons besoin de savoir quels formats propose
notre webcam. Tapez alors ceci : 


```
$ ffmpeg -f v4l2 -list_formats all -i /dev/video0
```


Vous obtiendrez un résultat comme : 

```
[video4linux2,v4l2 @ 0x2d76dd93800] Raw       :     yuyv422 :                 YUYV : 640x480 160x120 176x144 320x176 320x240 352x288 432x240 544x288 640x360 752x416 800x448 800x600 864x480 960x544 960x720 1024x576 1184x656 1280x720 1280x960
[video4linux2,v4l2 @ 0x2d76dd93800] Compressed:       mjpeg :                MJPEG : 640x480 160x120 176x144 320x176 320x240 352x288 432x240 544x288 640x360 752x416 800x448 800x600 864x480 960x544 960x720 1024x576 1184x656 1280x720 1280x960
```


Cela nous indique la liste des résolutions possibles et les formats vidéos
associés. 

Maintenant, nous allons configurer le serveur qui va se charger de diffuser la
vidéo. 

Copions le fichier de configuration dans ``/etc/ffserver.conf`` : 

```
# cp /usr/local/share/examples/ffmpeg/ffserver.conf /etc/ffserver.conf
```


Nous pouvons maintenant éditer ce fichier pour l'adapter à nos besoins. Je
recopie ci-dessous le fichier sans les commentaires : 

```
HTTPPort 8090
HTTPBindAddress 0.0.0.0
MaxHTTPConnections 2000
MaxClients 1000
MaxBandwidth 1000
CustomLog -

<Feed feed1.ffm>               
   File ./feed1.ffm           
   FileMaxSize 1G              
   ACL allow 127.0.0.1        # Acces seulement en local à ce fichier 
</Feed>

<Stream webcam.webm>       		
   Feed feed1.ffm              
   Format webm
   # Audio settings
   NoAudio
   # Video settings
   VideoCodec libvpx
   VideoSize 320x240           # Video resolution
   VideoFrameRate 25           # Video FPS
   AVOptionVideo flags +global_header  
   AVOptionVideo cpu-used 0
   AVOptionVideo qmin 5
   AVOptionVideo qmax 35
   AVOptionAudio flags +global_header
   PreRoll 15
   StartSendOnKey
</Stream>

<Stream status.html>     		# Server status URL
   Format status
   # Only allow local people to get the status
   ACL allow localhost
   ACL allow 192.168.0.0 192.168.255.255
</Stream>

<Redirect index.html>    # Just an URL redirect for index
   # Redirect index.html to the appropriate site
   URL http://www.ffmpeg.org/
</Redirect>
```



Notez que le serveur écoute par défaut sur le port 8090, il faudra donc penser à
l'ouvrir dans le parefeu.

Afin de lancer la diffusion, saisissez la commande suivante : 

```
ffmpeg -f v4l2 -video_size 320x240 -i /dev/video0 http://localhost:8090/feed1.ffm
```


Pour regarder votre flux, ouvrez un navigateur à l'adresse suivante : 

```
http://&NDD:8090/webcam.webm

```



Pour obtenir des informations sur le flux,
Vous pouvez aussi consulter la page ``http://&NDD:8090/status.html``



