Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


==Gestion des entêtes avec relayd==

Une gestion fine des entêtes peut vous intéresser. Cela peut notamment servir
pour indiquer aux navigateurs de garder en cache plus longtemps les fichiers
téléchargés et alléger la charge du serveur, ou encore régler des questions de
sécurité.

Httpd n'est pas capable de gérer les entêtes (ou //headers//). Heureusement,
tout est prévu : nous allons utiliser **relayd** et le placer //avant//
//httpd//.

Inutile d'installer quoi que ce soit, //relayd// est déjà présent dans OpenBSD. Elle
est pas belle la vie ?

La configuration de //relayd// est écrite dans le fichier ``/etc/relayd.conf`` que
nous allons éditer.

À l'intérieur, et à titre d'exemple, nous allons mettre les lignes suivantes :

```
ext_ip = &LOCALIPV4

http protocol "http" {
  tcp { nodelay, sack, socket buffer 65536, backlog 100 }
  match response header set "Cache-Control" value "max-age=1814400"
  match request header remove "Proxy"
  match response header set "X-Xss-Protection" value "1; mode=block"

  return error
  pass
}

relay "www" {
  listen on $ext_ip port 80
  protocol "http"
  forward to 127.0.0.1 port 8080 
}
```

Voici ce que ces lignes signifient : 

- ``ext_ip = &LOCALIPV4`` : Il s'agit de l'adresse 
[IP locale #IP] du serveur. 

- ``http protocol "http" {`` : On ouvre la configuration pour tout ce qui
  concerne le protocole http.
- ``tcp { nodelay, ...}`` : On ajoute quelques options pour sécuriser la
  connexion.
- ``match response header set "Cache-Control" ..."`` : On
  envoie un entête au visiteur du site qui
  augmente la valeur du cache, afin que les visiteurs de votre site gardent plus
  longtemps les données récupérées au lieu de les télécharger à nouveau de votre
  serveur. 
- ``match request header remove "Proxy"`` : On retire un entête qui peut poser
  des soucis de sécurité. Cela se produit à l'arrivée de la requête et permet de
  ne pas envoyer de mauvais entêtes au serveur //httpd//.
- ``match response header set "X-Xss-Protection" value "1;... "`` : On
  protège le site d'attaques XSS.
- ``return error`` : On renvoie une erreur s'il y a eu le moindre souci.
- ``pass`` : S'il n'y a pas eu de problèmes, on laisse passer.
- ``relay "www" {`` : On va définir ici la redirection vers le serveur.
- ``listen on $ext_ip port 80`` : On écoute sur le port 80, puisque c'est http.
- ``protocol "http"`` : On utilise la configuration énoncée plus haut.
- ``forward to 127.0.0.1 port 8080`` : On renvoie sur le port 8080 sur
  l'adresse interne du serveur (localhost). C'est sur ce port que //httpd// devra maintenant écouter.


Puisque //relayd// est relié à //httpd// par le port 8080, il faut modifier la
configuration de ce dernier pour lui dire d'écouter en local sur le bon port.
Dans le fichier ``/etc/httpd.conf``, nous aurons alors : 

```
listen on 127.0.0.1 port 8080
```

Après avoir réalisé vos modifications, n'oubliez pas d'activer //relayd// et de redémarrer les services : 

```
# rcctl enable relayd
# rcctl restart httpd
# rcctl start relayd
```

Désormais, lorsque vous accéderez à votre site, vous passerez par //relayd// (port
80) qui fera l'intermédiaire avec //httpd// (port 8080).

Vous pouvez consulter un exemple de configuration 
[à la fin du document #relaydconf].

**Notez cependant** que ces modifications sont valables sur l'ensemble de vos
sites, vous ne pouvez pas gérer les entêtes au cas par cas.


===Httpoxy===
Si votre site n'est accessible qu'en http (pas de chiffrement), alors je vous
conseille vivement de vous prémunir contre une faille connue sous le doux nom de 
[httpoxy https://httpoxy.org/]. Cela peut permettre (en gros hein...) à un pirate de paraître
venir de votre serveur lorsqu'il réalise une attaque. 

	Ou alors je met tout en https, c'est plus simple non ?

Oui ça serait plus simple et mieux pour la sécurité de vos visiteurs.

Voici à quoi ressemblera le fichier ``relayd.conf`` minimal pour s'en protéger : 

```
ext_ip = &LOCALIPV4

http protocol "http" {
    tcp { nodelay, sack, socket buffer 65536, backlog 100 }
    match request header remove "Proxy"
    return error
    pass
}

relay "www" {
    listen on $ext_ip port 80
    protocol "http"
    forward to 127.0.0.1 port 8080 
}
```


===Le cas https===
Si votre site propose une connexion chiffrée avec une adresse https://...,
(c'est bien ! ), la configuration de relayd peut-être déroutante.

Ci-dessous, voici un exemple de configuration de //relayd// qui renvoie tout ce qui
arrive sur le port 443 vers le port 8443 en local avec support tls : 

```
ext_ip = &LOCALIPV4

http protocol "https" {
  tcp { nodelay, sack, socket buffer 65536, backlog 100 }
  match response header set "Cache-Control" value "max-age=1814400"
  match request header remove "Proxy"
  match response header set "X-Xss-Protection" value "1; mode=block"

  return error
  pass
  tls { no client-renegotiation, cipher-server-preference }
}

relay "tlsforward" {
  listen on $ext_ip port 443 tls
  protocol "https"
  forward to 127.0.0.1 port 8443 check tcp
}
```

Désormais, dans la configuration de //httpd//, il n'y a plus de directives
relatives à tls, c'est //relayd// qui s'en charge. La configuration dans
httpd.conf contiendra donc : 

```
listen on localhost port 8443
hsts
```

Toutefois, il faut que //relayd// puisse vérifier les certificats. Afin qu'il y
accède, il faut les placer dans ``/etc/ssl/private/adresse:port.key`` pour la clé et
dans ``/etc/ssl/adresse:port.crt`` pour le certificat. Ici, "adresse" est l'IP locale
de votre serveur &LOCALIPV4.
Dans le cas où vous auriez récupéré un certificat avec 
[acme-client #acmeclient], vous pouvez alors faire des liens symboliques ainsi : 

```
# ln -s &SSLKEY /etc/ssl/private/&LOCALIPV4:443.key
# ln -s &SSLCERT /etc/ssl/&LOCALIPV4:443.crt
```

Inutile de préciser quoi que ce soit en plus dans la configuration de //relayd// ou
//httpd//, tout fonctionne normalement comme prévu avec l'utilisation de vos
certificats ;)

Si vous êtes en IPv6, la configuration se fait de la même façon. Veillez
toutefois à indiquer l'IPv6 complète (sans "::") dans la configuration de relayd
: 

```
ext_ipv6 =  2001:41d0:fe3c:6a00:0:0:0:1bad
```




Pour aller plus loin sur //relayd//, vous pouvez lire la page du wiki 
[OBSD4* correspondante https://wiki.obsd4a.net/doku.php?id=network:config:httpd_headers]





