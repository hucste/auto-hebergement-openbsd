Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


===Lecteur de flux RSS===
Vous pouvez installer sur votre serveur un outil qui vous permettra de lire en
un seul endroit les nouveautés publiées sur vos sites favoris grâce à leurs flux
[RSS https://fr.wikipedia.org/wiki/RSS]. C'est nettement plus pratique que
consulter les pages WEB une par une.

====KrISS : simple mais efficace====
KrISS est un lecteur de flux qui tient en un seul fichier PHP. Oui, un seul !
Pourtant, il est complet et rapide, tout ce qu'il faut pour une utilisation
personnelle.

Ceux qui souhaiteraient proposer un service d'agrégateur de flux à plusieurs
personnes pourront très bien installer KrISS dans différents dossiers, un par
utilisateur.

L'installation est simple comme tout, il faut télécharger le fichier de KrISS
puis le placer sur votre serveur web (http). 

- Préparez votre serveur pour [PHP #php] et ajoutez-y l'[extension curl #phppp].
- Téléchargez KrISS : 
```
ftp -o /tmp/index.php https://raw.github.com/tontof/kriss_feed/master/index.php
```
- Créez le dossier qui contiendra KrISS :
```
# mkdir -p /var/www/htdocs/kriss
```
- Déplacez le fichier index.php à l'endroit souhaité :

```
# mv /tmp/index.php /var/www/htdocs/kriss
```

- Ajustez les permissions : 
```
# chown -R www:daemon /var/www/htdocs/kriss
```

- Ajustez éventuellement la configuration de ``/etc/httpd.conf``, mais ça a sûrement déjà
  été fait lors de la configuration de [PHP #php] : 

```
server "&NDD" {
    root "/htdocs/kriss"
    listen on * tls port 443
    hsts
    tls {
        &SSLCERT
        &SSLKEY
    }
    location "*.php*" {
        fastcgi socket "/run/php-fpm.sock"
    }

    location "/kriss/" {
        directory index index.php
    }
}
```
- Relancez ``httpd`` avec ``rcctl reload httpd`` puis ouvrez votre navigateur sur l'emplacement de KrISS
  afin de terminer l'installation.


Profitez ;)

[img/kriss.png]


====TinyTinyRSS : application complète====
[Tiny Tiny RSS https://tt-rss.org] est un autre agrégateur de flux. Ce dernier est
très complet et conviendra davantage à ceux souhaitant proposer ce
service à plusieurs personnes. Toutefois, son installation est loin d'être
simple, et il s'avère souvent plus pratique d'avoir plusieurs instances de KrISS
à la place. Mais libre à vous d'utiliser votre outil favori.

Pour l'installer, vous devez avoir déjà : 
- Installé [PHP #php],
- Installé [PostgreSQL #postgresql] ou [MySQL #mysql] et créé une table pour Tiny Tiny RSS.


Nous allons utiliser PostgreSQL par souci de performance. 
On va créer une base uniquement pour Tiny Tiny RSS. Nous faisons le
choix de passer par un utilisateur ``ttrss`` qui aura accès à la base : 

```
# psql -U postgres -c "CREATE USER ttrss \
        WITH PASSWORD 'mot_de_passe_de_l_utilisateur';"

# psql -U postgres 
\connect template1
CREATE DATABASE "ttrssdb" WITH ENCODING 'UTF-8';
GRANT ALL PRIVILEGES ON DATABASE "ttrssdb" TO ttrss;
ALTER DATABASE "ttrssdb" OWNER TO ttrss;
\q
```


Nous pouvons enfin passer à la configuration de ttrss. On le télécharge dans le
dossier ``/var/www/htdocs/ttrss`` : 

```
# cd /var/www/htdocs
# git clone --depth=1 https://tt-rss.org/git/tt-rss.git ttrss
```


Ensuite, nous modifions les permissions pour le serveur web (http) : 

```
# chown -R www:daemon /var/www/htdocs/ttrss
```


On édite ``/etc/httpd.conf`` pour y ajouter notre nouveau site : 


```
server "rss.&NDD" {
    listen on * port 80
    block return 301 "https://$SERVER_NAME$REQUEST_URI"
    no log
}

server "rss.&NDD" {
    listen on * tls port 443
    root "/htdocs/ttrss"
    directory index index.php
    hsts
    tls {
        &SSLCERT
        &SSLKEY
    }

    location "*.php*" {
        fastcgi socket "/run/php-fpm.sock"
    }
}
```


On recharge la configuration de ``httpd`` : 

```
# rcctl reload httpd
```


Vous pouvez désormais terminer l'installation en ouvrant votre navigateur à
l'adresse ``https://rss.&NDD/install/``.

On vous demande les identifiants pour accéder à la base de données. Ce sont ceux
que vous avez créés juste avant pour l'utilisateur ``ttrss``.


[img/ttrssinstall1.png]

[img/ttrssinstall2.png]

Cliquez ensuite sur "Initialize database" : 

Cliquez sur "Save Configuration" pour l'enregistrer. Vous pouvez bien sûr
modifier cette configuration selon vos besoins avant.


Vous pouvez maintenant vous diriger à l'adresse ``http://rss.&NDD``
pour vérifier que tout fonctionne bien. Les identifiants par défaut sont "admin" et
"password", qu'il faudra changer au plus vite.

Si tout s'est bien passé, vous pouvez supprimer l'installateur : 

```
# rm -r /var/www/htdocs/ttrss/install
```


[img/ttrssinstall3.png]


Vous souhaiterez certainement mettre à jour régulièrement la liste des flux de
façon automatique. Si vous avez lu la documentation relative à TinyTinyRSS, vous
savez qu'il faut exécuter le fichier ``update.php``. Par exemple, dans une tâche
cron : 

```
@hourly /usr/local/bin/php-7.0 /var/www/htdocs/ttrss/update.php --feeds --quiet
```

